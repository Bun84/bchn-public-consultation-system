Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Visible progress on the items announced in initial plans for May & beyond (https://read.cash/@bitcoincashnode/bitcoin-cash-node-2020-plans-for-may-upgrade-and-beyond-11af0b52):
   - high prio: close the spec gap left by "automatic finalization" (rolling checkpoints)
2. Refinement of a project specific roadmap, based on somewhat consensual larger BCH roadmap
3. A successful crowdfunding campaign through Flipstarter and other means, setting the project on a sustainable development path
4. Fostering new developers in the community through #dev-newbie on BCHN slack
5. Much more feedback to the community about what's been done in the project
6. Creating a fully decentralized, many-platforms CI system using buildbot or similar
7. Much improved (and documented) QA procedures to catch more bugs, earlier
8. A product (node software) that is more modular / configurable for various users:
   - high prio: providing highly optimized mining node
   - a more decoupled GUI wallet - loosely coupled via API calls
   - optional modules for indexing, improved coin selection, chain branch monitoring & selection
   - providing pre-configured "spins" (a la Fedora) for mining, exchanges, home wallet users...
9. Close collaboration with BU + Verde's general BCH protocol specification efforts
10. Bitcoin Cash Node specific software requirements derived from general BCH requirement specification efforts
11. Some Bitcoin Cash Node software design documentation!
12. Long term: Moving critical parts of the software into a "correct-by-construction" (formally verified) development process


Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Increased adoption
2. Development and wider support in wallets for several voluntary funding systems
   (Flipstarter, U-DID, smart-contract systems like Mecenas, even "dumb" donation methods like built-in, configurable lists of projects for optional donations)
3. Robust, decentralized protocol client ecosystem
   - several clients used for mining and other critical node roles (pools, exchanges, payment processing)
   - these clients written in different programming languages and well documented
4. Development of an excellent set of protocol specifications (solid standards around which industry can coalesce and upon which it can build)
5. Emergence of a 'Bitcoin Cash Testing Project' effort similar to the Linux Testing Project


Section C - Information about yourself
--------------------------------------
This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [x] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a BCH user
- [ ] - other - please specify: ....................................
