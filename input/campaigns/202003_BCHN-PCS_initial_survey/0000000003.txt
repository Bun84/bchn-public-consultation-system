# Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project

1. A clear policy on how money is going to be accounted for. Who gets them? For what proposed role? How much? In BCH or USD? Where do we see if this is carried out on an ongoing basis?

2. How you guys are different from ABC in terms of technical direction, in maintenance, deploying new features, testing and update schedules.

3. You guys mentioned you're going to be more collaborative with other node implementations. How do you plan to keep progress? The more teams participate in discussions, the more diverse an opinion there will be, and the harder it will be to reach any consensus in opinion. There will obviously be opinion that needs to be discounted - how do you plan to do that while remaining collaborative in general?

4. A clear, believable plan in terms of personnel and timeline, in achieving any deliverables whether maintenance or new features.

# Section B - What you wish to see in Bitcoin Cash (BCH) overall

1. An overall strategy in getting from here to mass adoption. Technical "roadmap" is not that, technical improvements merely enable the actual strategies.

2. Clear reiteration from developers, miners and other prominent actors about which principles should never be violated in Bitcoin Cash.

3. An ecosystemwide decisionmaking process, to minimize future community fracture from disagreements.

4. A track record of voluntary funding for valuable infrastructure projects.

5. High signal, easily accessible analysis sources for technical, business and economic issues within the ecosystem, above and beyond regular news outlets.

6. A robust testnet, or layers of testnet, that provides confidence in upgrades, new features and research findings.

# Section C - Information about yourself

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [x] - a Bitcoin Cash protocol or full node client developer
- [x] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a person with significant amounts of their long term asset holdings in BCH
- [x] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [ ] - other - please specify: ....................................
